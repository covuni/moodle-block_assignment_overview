<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains language strings used for the Assignment Overview block.
 *
 * @package block_assignment_overview
 * @copyright 2019 Manoj Solanki
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Assignment Overview';
$string['blocktitle'] = 'Assignment Overview';

$string['assignment_overview:addinstance'] = 'Add an Assignment Overview block';
$string['assignment_overview:myaddinstance'] = 'Add an Assignment Overview block to the My Moodle page.';

$string['title'] = 'Assignment Overview';
$string['titledesc'] = 'Title for Assignment Overview block.  Leave empty to not show one (default).';

$string['assignments_col_header'] = 'Assignments due';
$string['nocurrentassignments'] = 'No current assignments';

$string['assignmenttabtext'] = 'These are your assignments that have been set up in Moodle';
$string['assignmenttabmodulecol'] = 'Module';
$string['assignmenttabasscol'] = 'Assignments';


$string['scrollablecontainer'] = 'Max height with scrollbar';
$string['scrollablecontainerdesc'] = 'Select this to have the content a maximum height, with a scrollbar where needed. This is useful in the case of a long list of assignments,'
                                     . ' to avoid the content extending the height of the page excessively. The max height is fixed in the CSS code. '
                                     . ' This is done in a CSS class called "block-assignments-overview-container-scrollable" and could be overridden in '
                                     . ' many themes if a different max height is desired.';

$string['assignmentstoptext'] = 'Assignment title text';
$string['assignmentstoptextdesc'] = 'The text you want to appear at the top.';

$string['privacy:metadata'] = 'The Assignments Overview block does not store or collect any data.';

$string['furtherinformationincludesubmissionfeedback'] = 'Include Submission / Feedback information';
$string['furtherinformationincludesubmissionfeedbackdesc'] = 'Include Submission / Feedback information for teachers and students';

$string['assignmentsduethreshold'] = 'Assignments due threshold';
$string['assignmentsduethresholddesc'] = 'Display assignments due that are no older (in days) than this value.';

$string['includeturnitintooltwoassignments'] = 'Include Turnitin Tool Two assignments';
$string['includeturnitintooltwoassignmentsdesc'] = 'Also show Turnitin Tool Two assignments in the list of assignments.';

$string['filterdefault'] = 'Default value for main filter';
$string['filterdefaultdesc'] = 'Default value for main filter when no user preference is set. This is useful if you want users' .
                                ' to see a particular default filter before one is set.';

$string['assignmentsduetitle'] = "Assignments due";
$string['pastassignmentssubmittedtitle'] = "Assignments submitted";
$string['pastassignmentsnotsubmittedtitle'] = 'Past Assignments not submitted';
$string['assignmentsnoduedatetitle'] = 'Assignments with no due date';
$string['assignmentsalltitle'] = 'All Assignments';

$string['emptytextassignmentsdue'] = 'No due assignments found';
$string['emptytextpastassignmentssubmitted'] = 'No past submitted assignments found';
$string['emptytextpastassignmentsnotsubmitted'] = 'No past assignments found that have not been submitted';
$string['emptytextassignmentsnoduedate'] = 'No assignments found with no due date';
$string['emptytextassignmentsall'] = 'No assignments found';

// Aria controls.
$string['aria:addtofavourites'] = 'Star for';
$string['aria:allcourses'] = 'All courses';
$string['aria:card'] = 'Switch to card view';
$string['aria:controls'] = 'Course overview controls';
$string['aria:courseactions'] = 'Actions for current course';
$string['aria:coursesummary'] = 'Course summary text:';
$string['aria:courseprogress'] = 'Course progress:';
$string['aria:displaydropdown'] = 'Display drop-down menu';
$string['aria:favourites'] = 'Show starred courses';
$string['aria:future'] = 'Show future courses';
$string['aria:assignmentfilterdropdown'] = 'Assignment filter drop-down menu';
$string['aria:inprogress'] = 'Show in courses in progress';
$string['aria:lastaccessed'] = 'Sort courses by last accessed date';
$string['aria:list'] = 'Switch to list view';
$string['aria:title'] = 'Sort courses by course name';
$string['aria:past'] = 'Show past courses';
$string['aria:removefromfavourites'] = 'Remove star for';
$string['aria:summary'] = 'Switch to summary view';
$string['aria:groupingdropdown'] = 'Sorting drop-down menu';

// Activity display *********************************.
$string['answered'] = 'Answered';
$string['attempted'] = 'Attempted';
$string['contributed'] = 'Contributed';
$string['draft'] = 'Not published to students';
$string['due'] = 'Due {$a}';
$string['gradeavailable'] = 'Grade available';
$string['gradenotavailable'] = 'Grade not available';
$string['feedbackavailable'] = 'Feedback available';
$string['feedbacknotavailable'] = 'No Feedback available';
$string['submissionnotrequired'] = 'Online submission not required';
$string['notanswered'] = 'Not answered';
$string['notattempted'] = 'Not attempted';
$string['notcontributed'] = 'Not contributed';
$string['notsubmitted'] = 'Not Submitted';
$string['overdue'] = 'Overdue';
$string['reopened'] = 'Reopened';
$string['submitted'] = 'Submitted';

$string['xofyanswered'] = '{$a->completed} of {$a->participants} Answered';
$string['xofyattempted'] = '{$a->completed} of {$a->participants} Attempted';
$string['xofycontributed'] = '{$a->completed} of {$a->participants} Contributed';
$string['xofysubmitted'] = '{$a->completed} of {$a->participants} Submitted';
$string['xungraded'] = '{$a} Ungraded';

/*
 * Cache settings.
 */
$string['usecaching'] = 'Use caching';
$string['usecachingdesc'] = 'Switch on caching (uses Moodle cache API)';
$string['cachingttl'] = 'Caching expiry time';
$string['cachingttldesc'] = 'Caching expiry time in seconds (TTL)';

// Activity display *********************************.
$string['answered'] = 'Answered';
$string['attempted'] = 'Attempted';
$string['contributed'] = 'Contributed';
$string['draft'] = 'Not published to students';
$string['due'] = 'Due {$a}';
$string['gradeavailable'] = 'Grade available';
$string['gradenotavailable'] = 'Grade not available';
$string['feedbackavailable'] = 'Feedback available';
$string['feedbacknotavailable'] = 'No Feedback available';
$string['submissionnotrequired'] = 'Online submission not required';
$string['notanswered'] = 'Not answered';
$string['notattempted'] = 'Not attempted';
$string['notcontributed'] = 'Not contributed';
$string['notsubmitted'] = 'Not submitted';
$string['overdue'] = 'Overdue';
$string['reopened'] = 'Reopened';
$string['submitted'] = 'Submitted';

$string['xofyanswered'] = '{$a->completed} of {$a->participants} Answered';
$string['xofyattempted'] = '{$a->completed} of {$a->participants} Attempted';
$string['xofycontributed'] = '{$a->completed} of {$a->participants} Contributed';
$string['xofysubmitted'] = '{$a->completed} of {$a->participants} Submitted';
$string['xungraded'] = '{$a} Ungraded';
