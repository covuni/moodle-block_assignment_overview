// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Javascript to initialise the assignment overview block.
 *
 * @package    block_assignment_overview
 * @copyright  2019 Manoj Solanki (Coventry University)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define(
[
    'jquery',
    'core/ajax',
    'core/custom_interaction_events'
],
function(
    $,
    Ajax,
    CustomEvents
) {

    var assignmentsView = {
        region: '[data-region="assignments-overview"]'
    };

    var SELECTORS = {
        FILTERS: '[data-region="filter"]',
        FILTER_OPTION: '[data-filter]',
        DISPLAY_OPTION: '[data-display-option]'
    };

    /**
     * Initialise all of the modules for the overview block.
     *
     * @param {object} root The root element for the overview block.
     */
    var init = function(root, currentfilter, filterslist) {
        root = $(root);

        registerSelector(root, filterslist);

        var toggle = "#" + currentfilter;

        $(toggle).show();
    };

    /**
     * Event listener for the Assignments filter (assignmentsdue, pastassignmentssubmitted, pastassignmentsnotsubmitted).
     *
     * @param {object} root The root element for the assignment overview block
     */
    var registerSelector = function(root, filters) {

        var Selector = root.find(SELECTORS.FILTERS);

        CustomEvents.define(Selector, [CustomEvents.events.activate]);
        Selector.on(
            CustomEvents.events.activate,
            SELECTORS.FILTER_OPTION,
            function(e, data) {
                var option = $(e.target);
                if (option.hasClass('active')) {
                    // If it's already active then we don't need to do anything.
                    return;
                }

                var filter = option.attr('data-filter');
                var pref = option.attr('data-pref');

                root.find(assignmentsView.region).attr('data-' + filter, option.attr('data-value'));
                updatePreferences({
                    preferences: [{
                        type: 'block_assignment_overview_user_filter_preference',
                        value: pref
                    }]
                });

                filters.forEach(function(filter) {
                    if (filter == pref) {
                        $("#" + filter).show(500);
                    } else {
                        $("#" + filter).hide(100);
                    }
                });

                data.originalEvent.preventDefault();
            }
        );

    };

    /**
     * Update the user preferences.
     *
     * @param {Object} args Arguments send to the webservice.
     *
     * Sample args:
     * {
     *     preferences: [
     *         {
     *             type: 'block_example_user_sort_preference'
     *             value: 'title'
     *         }
     *     ]
     * }
     */
    var updatePreferences = function(args) {
        var request = {
            methodname: 'core_user_update_user_preferences',
            args: args
        };

        Ajax.call([request])[0]
            .fail(Notification.exception);
    };

    return {
        init: init
    };
});
