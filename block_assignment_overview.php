<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Moodle Assignment overview.
 *
 * Aggregates and displays a list of all assignments for the current user, including
 * due date and other information.  Only displays on the dashboard or frontpage.
 *
 * @package block_assignment_overview
 * @copyright 2019 Manoj Solanki (Coventry University)
 *
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

defined('MOODLE_INTERNAL') || die;

require_once(dirname(__FILE__) . '/lib.php');

/**
 * Assignments Overview block implementation class.
 *
 * @package   block_assignment_overview
 * @copyright 201 Manoj Solanki (Coventry University)
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_assignment_overview extends block_base {

    /** @var array The plugin name of the block */
    const BLOCKPLUGINNAME = 'block_assignment_overview';

    /**
     * @var CACHENAME_ASSIGNMENTSOVERVIEW  The name of the cache used for storing assignment information for a user.
     */
    const CACHENAME_ASSIGNMENTSOVERVIEW = 'assignmentsoverviewdata';

    /**
     * @var CACHENAME_ASSIGNMENTSOVERVIEW_KEY  The key of the cache used for storing assignment information for a user.
     */
    const CACHENAME_ASSIGNMENTSOVERVIEW_KEY = 'assignmentsoverviewkey';

    /**
     * Adds title to block instance.
     */
    public function init() {
        $this->title = get_string('pluginname', 'block_assignment_overview');
    }

    /**
     * Set up any configuration data.
     *
     * The is called immediatly after init().
     */
    public function specialization() {
        $config = get_config("block_assignment_overview");

        // Use the title as defined in plugin settings, if one exists.
        if (!empty($config->title)) {
            $this->title = $config->title;
        } else {
            $this->title = get_string('pluginname', 'block_assignment_overview');
        }
    }

    /**
     * Which page types this block may appear on.  This currently is set to allow it to display
     * on the front page, dashboard page and the course home page.
     */
    public function applicable_formats() {
        return array('site-index' => true, 'my' => true);
    }

    /**
     * Get block instance content.
     */
    public function get_content() {
        global $CFG, $COURSE, $USER, $OUTPUT, $PAGE, $ME, $DB;

        if ($this->content !== null) {
            return $this->content;
        }

        $config = get_config("block_assignment_overview");

        if (!isloggedin()) {
            return $this->content;
        }

        $this->content = new stdClass;
        $this->content->text = '';

        // Do not display on any course pages except the main course.
        if ($COURSE->id != 1) {
            $this->content = '';
            return $this->content;
        }

        $this->content->text .= '<!-- Assignments Overview -->';

        $customfields = profile_user_record($USER->id);

        // Check if caching is being used.
        if (!empty ($config->usecaching)) {
            $cache = cache::make('block_assignment_overview', self::CACHENAME_ASSIGNMENTSOVERVIEW);

            $returnedcachedata = $cache->get(self::CACHENAME_ASSIGNMENTSOVERVIEW_KEY);

            // Use this to write data to cache in array format, ['lastcachebuildtime'] = last access time, ['data'] = actual data.
            $cachedatastore = array();

            $usercachettl = $config->cachingttl;

            $timenow = time();

            // If no data retrieved or lastcachebuildtime has no value.
            // Or if user's last cache has expired since it was last built.
            if ( ($returnedcachedata === false) || (!isset($returnedcachedata['lastcachebuildtime'])) ||
                    ( $timenow > ($returnedcachedata['lastcachebuildtime'] + $usercachettl)) ) {

                $cachedatastore['data'] = block_assignment_overview_get_all_assignments(enrol_get_all_users_courses(
                                            $USER->id, true, 'id, shortname',
                                            'visible DESC, sortorder ASC'), array(), $customfields);

                // Now timestamp the cache with last build time.
                $cachedatastore['lastcachebuildtime'] = time();
                $cache->set(self::CACHENAME_ASSIGNMENTSOVERVIEW_KEY, $cachedatastore);
            } else {
                $cachedatastore['data'] = $returnedcachedata['data'];  // We got valid, non-expired data from cache.
            }

            // Final check to ensure there is data in $cachedatastore.
            if (isset ($cachedatastore['data'])) {
                $allassignments = $cachedatastore['data'];
            } else {
                $allassignments = $cachedatastore['data'] = block_assignment_overview_get_all_assignments(
                                                        enrol_get_all_users_courses($USER->id, true, 'id, shortname',
                                                        'visible DESC, sortorder ASC'), array(), $customfields);
            }
        } else {
            $allassignments = block_assignment_overview_get_all_assignments(enrol_get_all_users_courses(
                            $USER->id, true, 'id, shortname',
                            'visible DESC, sortorder ASC'), array(), $customfields);
        }

        $assignmentsdue = array();
        $pastassignmentssubmitted = array();
        $pastassignmentsnotsubmitted = array();
        $assignmentsnoduedate = array();
        $assignmentsall = array();

        foreach ($allassignments as $assignment) {
            if (!empty($assignment['displaytype'])) {
                if ($assignment['displaytype'] == 'assignmentsdue') {
                    $assignmentsdue[] = $assignment;
                } else if ($assignment['displaytype'] == 'pastassignmentsnotsubmitted') {
                    $pastassignmentsnotsubmitted[] = $assignment;
                } else if ($assignment['displaytype'] == 'pastassignmentssubmitted') {
                    $pastassignmentssubmitted [] = $assignment;
                } else if ($assignment['displaytype'] == 'assignmentsnoduedate') {
                    $assignmentsnoduedate[] = $assignment;
                }
                $assignmentsall[] = $assignment;
            }
        }

        // Order assignments by most recent due date now.
        usort($assignmentsdue, function ($a, $b) {
            return $a['duedate'] > $b['duedate'];
        });

        usort($pastassignmentsnotsubmitted, function ($a, $b) {
            return $a['duedate'] > $b['duedate'];
        });

        usort($pastassignmentssubmitted, function ($a, $b) {
            return $a['duedate'] > $b['duedate'];
        });

        usort($assignmentsall, function ($a, $b) {
            return $a['duedate'] > $b['duedate'];
        });

        // We have a custom user preference for the filter selection.
        $filter = get_user_preferences('block_assignment_overview_user_filter_preference');

        if (!$filter) {
            if (!empty ($config->filterdefault)) {
                $filter = $config->filterdefault;
            } else {
                $filter = BLOCK_ASSIGNMENTS_OVERVIEW_ASSIGNMENTS_DUE;
            }
        }

        $filterpreferences = array(
                BLOCK_ASSIGNMENTS_OVERVIEW_ASSIGNMENTS_DUE =>
                    (BLOCK_ASSIGNMENTS_OVERVIEW_ASSIGNMENTS_DUE == $filter ? true : false),
                BLOCK_ASSIGNMENTS_OVERVIEW_PAST_ASSIGNMENTS_SUBMITTED =>
                    (BLOCK_ASSIGNMENTS_OVERVIEW_PAST_ASSIGNMENTS_SUBMITTED == $filter ? true : false),
                BLOCK_ASSIGNMENTS_OVERVIEW_PAST_ASSIGNMENTS_NOT_SUBMITTED =>
                    (BLOCK_ASSIGNMENTS_OVERVIEW_PAST_ASSIGNMENTS_NOT_SUBMITTED == $filter ? true : false),
                BLOCK_ASSIGNMENTS_OVERVIEW_ASSIGNMENTS_NO_DUE_DATE =>
                (BLOCK_ASSIGNMENTS_OVERVIEW_ASSIGNMENTS_NO_DUE_DATE == $filter ? true : false),
                BLOCK_ASSIGNMENTS_OVERVIEW_ASSIGNMENTS_ALL =>
                (BLOCK_ASSIGNMENTS_OVERVIEW_ASSIGNMENTS_ALL == $filter ? true : false)
        );

        $filterslist = array (BLOCK_ASSIGNMENTS_OVERVIEW_ASSIGNMENTS_DUE, BLOCK_ASSIGNMENTS_OVERVIEW_PAST_ASSIGNMENTS_SUBMITTED,
                BLOCK_ASSIGNMENTS_OVERVIEW_PAST_ASSIGNMENTS_NOT_SUBMITTED, BLOCK_ASSIGNMENTS_OVERVIEW_ASSIGNMENTS_NO_DUE_DATE,
                BLOCK_ASSIGNMENTS_OVERVIEW_ASSIGNMENTS_ALL);

        // Build an array containing all the data.
        $assignmentcontent = '';
        $templatedatajson = new stdClass();
        $assignmentsarray = array();
        $assignmentsarray[] = array("id" => "assignmentsdue", "assignments" => $assignmentsdue,
                                    "class" => $filterpreferences[BLOCK_ASSIGNMENTS_OVERVIEW_ASSIGNMENTS_DUE] ? '' : '',
                                    "emptytext" => empty($assignmentsdue)
                                    ? get_string('emptytextassignmentsdue', 'block_assignment_overview') : '');

        $assignmentsarray[] = array("id" => "pastassignmentssubmitted", "assignments" => $pastassignmentssubmitted,
                                    "class" => $filterpreferences[BLOCK_ASSIGNMENTS_OVERVIEW_PAST_ASSIGNMENTS_SUBMITTED] ? '' : '',
                                    "emptytext" => empty($pastassignmentssubmitted)
                                    ? get_string('emptytextpastassignmentssubmitted', 'block_assignment_overview') : '');

        $assignmentsarray[] = array("id" => "pastassignmentsnotsubmitted", "assignments" => $pastassignmentsnotsubmitted,
                                    "class" =>
                                    $filterpreferences[BLOCK_ASSIGNMENTS_OVERVIEW_PAST_ASSIGNMENTS_NOT_SUBMITTED] ? '' : '',
                                    "emptytext" => empty($pastassignmentsnotsubmitted)
                                    ? get_string('emptytextpastassignmentsnotsubmitted', 'block_assignment_overview') : '');

        $assignmentsarray[] = array("id" => "assignmentsnoduedate", "assignments" => $assignmentsnoduedate,
                                    "class" => $filterpreferences[BLOCK_ASSIGNMENTS_OVERVIEW_ASSIGNMENTS_NO_DUE_DATE] ? '' : '',
                                    "emptytext" => empty($assignmentsnoduedate)
                                    ? get_string('emptytextassignmentsnoduedate', 'block_assignment_overview') : '');

        $assignmentsarray[] = array("id" => "assignmentsall", "assignments" => $assignmentsall,
                                    "class" => $filterpreferences[BLOCK_ASSIGNMENTS_OVERVIEW_ASSIGNMENTS_ALL] ? '' : '',
                                    "emptytext" => empty($allassignments)
                                    ? get_string('emptytextassignmentsall', 'block_assignment_overview') : '');

        $uniqid = html_writer::random_id('block-assignments-overview-');

        // If any title text is configured.
        $title = '';
        if (!empty($config->assignmentstoptext)) {
            $title = $config->assignmentstoptext;
        }

        // Display optional scrollbar. Useful in the case where there are lots of assignments.
        $scrollable = false;
        if (!empty($config->scrollablecontainer)) {
            $scrollable = true;
        }

        $templatedatajson = array ("uniqid" => $uniqid, "preference" => $filterpreferences, "data" => $assignmentsarray,
                                   "scrollable" => $scrollable, "title" => $title);
        $this->content->text .= $OUTPUT->render_from_template('block_assignment_overview/assignments', $templatedatajson);

        $this->page->requires->js_call_amd('block_assignment_overview/main', 'init',
                                            array('root' => '#' . $uniqid, 'filter' => $filter, 'filterslist' => $filterslist));

        $this->content->text .= '<!-- End of Assignment Overview Content -->';

        return $this->content;
    }

    /**
     * Allow the block to have a configuration page
     *
     * @return boolean
     */
    public function has_config() {
        return true;
    }

    /**
     * Allow multiple instances of the block.
     */
    public function instance_allow_multiple() {
        return false;
    }

    /**
     * Sets block header to be hidden or visible
     *
     * @return bool if true then header will not be visible.
     */
    public function hide_header() {
         return true;
    }

}
