<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * Settings for Assignment Overview block.
 *
 * @package   block_assignment_overview
 * @copyright 2019 Manoj Solanki (Coventry University)
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

defined('MOODLE_INTERNAL') || die;

$mainfilterchoices = array (
    BLOCK_ASSIGNMENTS_OVERVIEW_ASSIGNMENTS_DUE => get_string('assignmentsduetitle', 'block_assignment_overview'),
    BLOCK_ASSIGNMENTS_OVERVIEW_PAST_ASSIGNMENTS_SUBMITTED => get_string('pastassignmentssubmittedtitle',
        'block_assignment_overview'),
    BLOCK_ASSIGNMENTS_OVERVIEW_PAST_ASSIGNMENTS_NOT_SUBMITTED => get_string('pastassignmentsnotsubmittedtitle',
        'block_assignment_overview'),
    BLOCK_ASSIGNMENTS_OVERVIEW_ASSIGNMENTS_NO_DUE_DATE => get_string('assignmentsnoduedatetitle', 'block_assignment_overview'),
    BLOCK_ASSIGNMENTS_OVERVIEW_ASSIGNMENTS_ALL => get_string('assignmentsalltitle', 'block_assignment_overview')
);

if ($ADMIN->fulltree) {

    $settings->add(new admin_setting_configcheckbox('block_assignment_overview' . '/usecaching',
        get_string('usecaching', 'block_assignment_overview'),
        get_string('usecachingdesc', 'block_assignment_overview'), 0));

    $settings->add(new admin_setting_configtext('block_assignment_overview' . '/cachingttl',
        get_string('cachingttl', 'block_assignment_overview'),
        get_string('cachingttldesc', 'block_assignment_overview'), ASSIGNMENTS_OVERVIEW_CACHE_TTL_DEFAULT, PARAM_INT));

    $settings->add(new admin_setting_configtext('block_assignment_overview' . '/assignmentstoptext',
        get_string('assignmentstoptext', 'block_assignment_overview'),
        get_string('assignmentstoptextdesc', 'block_assignment_overview'), '', PARAM_TEXT));

    $settings->add(new admin_setting_configcheckbox('block_assignment_overview' . '/scrollablecontainer',
        get_string('scrollablecontainer', 'block_assignment_overview'),
        get_string('scrollablecontainerdesc', 'block_assignment_overview'), 0));

    $settings->add(new admin_setting_configcheckbox('block_assignment_overview' . '/furtherinformationincludesubmissionfeedback',
        get_string('furtherinformationincludesubmissionfeedback', 'block_assignment_overview'),
        get_string('furtherinformationincludesubmissionfeedbackdesc', 'block_assignment_overview'), 0));

    // The threshold for due assignments (e.g. assignments not older than this value).
    $settings->add(new admin_setting_configtext('block_assignment_overview' . '/assignmentsduethreshold',
        get_string('assignmentsduethreshold', 'block_assignment_overview'),
        get_string('assignmentsduethresholddesc', 'block_assignment_overview'),
        ASSIGNMENTS_OVERVIEW_THRESHOLD_ASSIGNMENTS_DUE_DEFAULT, PARAM_INT));

    // Get Turnitn tool two assignments.
    $settings->add(new admin_setting_configcheckbox('block_assignment_overview' . '/includeturnitintooltwoassignments',
        get_string('includeturnitintooltwoassignments', 'block_assignment_overview'),
        get_string('includeturnitintooltwoassignmentsdesc', 'block_assignment_overview'), 0));

    $settings->add(new admin_setting_configselect('block_assignment_overview/filterdefault',
        get_string('filterdefault', 'block_assignment_overview'),
        get_string('filterdefaultdesc', 'block_assignment_overview'),
        BLOCK_ASSIGNMENTS_OVERVIEW_ASSIGNMENTS_DUE, $mainfilterchoices));
}