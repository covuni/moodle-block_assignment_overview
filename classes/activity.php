<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Activity-related functions.
 *
 * This defines the activity class that is used to retrieve activity-related information, such as submission status,
 * due dates etc.
 *
 * @package block_assignment_overview
 * @copyright 2019 Manoj Solanki (Coventry University)
 *
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

namespace block_assignment_overview;

defined('MOODLE_INTERNAL') || die();

use \block_assignment_overview\activity_meta;
use cm_info;
use context_course;
use html_writer;
use moodle_url;

require_once($CFG->dirroot.'/mod/assign/locallib.php');

/**
 * Activity functions.
 *
 * These functions are in a class purely for auto loading convenience.
 *
 * @package   block_assignment_overview
 * @copyright Copyright (c) 2015 Moodlerooms Inc. (http://www.moodlerooms.com)
 * @copyright Copyright (c) 2019 Manoj Solanki (Coventry University)
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class activity {

    /**
     * Main method that calls relevant activity-related method based on the mod name.
     *
     * @param \cm_info $mod
     * @param array    $tiitwodata
     * @return activity_meta
     */
    public static function module_meta(\cm_info $mod, $tiitwodata = array()) {
        $methodname = $mod->modname . '_meta';
        if (method_exists('block_assignment_overview\\activity', $methodname)) {
            if (empty($tiitwodata) ) {
                $meta = call_user_func('block_assignment_overview\\activity::' . $methodname, $mod);
            } else {
                $meta = call_user_func('block_assignment_overview\\activity::' . $methodname, $mod, $tiitwodata);
            }
        } else {
            $meta = new activity_meta(); // Return empty activity meta.
        }
        return $meta;
    }

    /**
     * Return standard meta data for module
     *
     * @param \cm_info $mod
     * @param string $timeopenfld
     * @param string $timeclosefld
     * @param string $keyfield
     * @param string $submissiontable
     * @param string $submittedonfld
     * @param string $submitstrkey
     * @param bool $isgradeable
     * @param string $submitselect - sql to further filter submission row select statement - e.g. st.status='finished'
     * @param bool $submissionnotrequired
     * @return activity_meta
     */
    protected static function std_meta(\cm_info $mod,
            $timeopenfld,
            $timeclosefld,
            $keyfield,
            $submissiontable,
            $submittedonfld,
            $submitstrkey,
            $isgradeable = false,
            $submitselect = '',
            $submissionnotrequired = false
            ) {
        global $USER, $CFG;

        $config = get_config("block_assignment_overview");
        $courseid = $mod->course;

        // Create meta data object.
        $meta = new activity_meta();
        $meta->submissionnotrequired = $submissionnotrequired;
        $meta->submitstrkey = $submitstrkey;
        $meta->submittedstr = get_string($submitstrkey, 'block_assignment_overview');
        $meta->notsubmittedstr = get_string('not'.$submitstrkey, 'block_assignment_overview');
        if (get_string_manager()->string_exists($mod->modname.'draft', 'block_assignment_overview')) {
            $meta->draftstr = get_string($mod->modname.'draft', 'block_assignment_overview');
        } else {
            $meta->drafstr = get_string('draft', 'block_assignment_overview');
        }

        if (get_string_manager()->string_exists($mod->modname.'reopened', 'block_assignment_overview')) {
            $meta->reopenedstr = get_string($mod->modname.'reopened', 'block_assignment_overview');
        } else {
            $meta->reopenedstr = get_string('reopened', 'block_assignment_overview');
        }

        // If module is not visible to the user then don't bother getting meta data.
        if (!$mod->uservisible) {
            return $meta;
        }

        $activitydates = self::instance_activity_dates($courseid, $mod, $timeopenfld, $timeclosefld);
        $meta->timeopen = $activitydates->timeopen;
        $meta->timeclose = $activitydates->timeclose;
        if (isset($activitydates->extension)) {
            $meta->extension = $activitydates->extension;
        }

        // If role has specific "teacher" capabilities.
        if (has_capability('mod/assign:grade', $mod->context)) {
            $meta->isteacher = true;
            // Teacher - useful teacher meta data.
            $methodnsubmissions = $mod->modname.'_num_submissions';
            $methodnungraded = $mod->modname.'_num_submissions_ungraded';

            if (method_exists('block_assignment_overview\\activity', $methodnsubmissions)) {
                $meta->numsubmissions = call_user_func('block_assignment_overview\\activity::'.
                    $methodnsubmissions, $courseid, $mod->instance);
            }
            if (method_exists('block_assignment_overview\\activity', $methodnungraded)) {
                $meta->numrequiregrading = call_user_func('block_assignment_overview\\activity::'.
                    $methodnungraded, $courseid, $mod->instance);
            }

        } else {
            // Student - useful student meta data - only display if activity is available.
            if (empty($activitydates->timeopen) || $activitydates->timeopen <= time()) {

                $submissionrow = '';

                $submissionrow = self::get_submission_row($courseid, $mod, $submissiontable, $keyfield, $submitselect);

                if (!empty($submissionrow)) {
                    if ($mod->modname === 'assign' && !empty($submissionrow->status)) {

                        switch ($submissionrow->status) {
                            case ASSIGN_SUBMISSION_STATUS_DRAFT:
                                $meta->draft = true;
                                break;

                            case ASSIGN_SUBMISSION_STATUS_REOPENED:
                                $meta->reopened = true;
                                break;

                            case ASSIGN_SUBMISSION_STATUS_SUBMITTED:
                                $meta->submitted = true;
                                break;
                        }
                    } else {
                        $meta->submitted = true;
                        $meta->timesubmitted = !empty($submissionrow->$submittedonfld) ? $submissionrow->$submittedonfld : null;
                    }
                    // If submitted on field uses modified field then fall back to timecreated if modified is 0.
                    if (empty($meta->timesubmitted) && $submittedonfld = 'timemodified') {
                        if (isset($submissionrow->timemodified)) {
                            $meta->timesubmitted = $submissionrow->timemodified;
                        } else {
                            $meta->timesubmitted = $submissionrow->timecreated;
                        }
                    }
                }
            }

            if (isset($config->furtherinformationincludesubmissionfeedback)
                && $config->furtherinformationincludesubmissionfeedback == true) {
                $graderow = false;
                if ($isgradeable) {
                    $graderow = self::grade_row($courseid, $mod);
                }

                if ($graderow) {
                    $gradeitem = \grade_item::fetch(array(
                            'itemtype' => 'mod',
                            'itemmodule' => $mod->modname,
                            'iteminstance' => $mod->instance,
                    ));

                    $grade = new \grade_grade(array('itemid' => $gradeitem->id, 'userid' => $USER->id));

                    $coursecontext = \context_course::instance($courseid);
                    $canviewhiddengrade = has_capability('moodle/grade:viewhidden', $coursecontext);

                    if (!$grade->is_hidden() || $canviewhiddengrade) {
                        $meta->grade = true;
                    }
                }
            }
        }

        if (!empty($meta->timeclose)) {
            // Submission required?
            $subreqd = empty($meta->submissionnotrequired);

            // Overdue?
            $meta->overdue = $subreqd && empty($meta->submitted) && (time() > $meta->timeclose);
        }

        return $meta;
    }

    /**
     * Get TII assignment meta data.
     *
     * @param \cm_info $mod
     * @param array   $tiitwodata
     * @return activity_meta
     */
    public static function turnitintooltwo_meta(\cm_info $mod, $tiitwodata = array()) {
        global $DB, $USER;

        $config = get_config("block_assignment_overview");
        $courseid = $mod->course;
        static $submissionsenabled = array();

        $submitselect = '';

        // If there aren't any submission plugins enabled for this module, then submissions are not required.
        if (empty($submissionsenabled[$courseid][$mod->instance])) {
            $submissionnotrequired[$courseid] = true;
        } else {
            $submissionnotrequired[$courseid] = false;
        }

        // Get meta data. A minor amount of this code is duplicated from std_meta (tried to avoid this!).

        // Create meta data object.
        $meta = new activity_meta();
        $meta->submissionnotrequired = 0;
        $meta->name = $tiitwodata['name'];

        $meta->submittedstr = get_string('submitted', 'block_assignment_overview');
        $meta->notsubmittedstr = get_string('notsubmitted', 'block_assignment_overview');
        if (get_string_manager()->string_exists($mod->modname.'draft', 'block_assignment_overview')) {
            $meta->draftstr = get_string($mod->modname.'draft', 'block_assignment_overview');
        } else {
            $meta->drafstr = get_string('draft', 'block_assignment_overview');
        }

        if (get_string_manager()->string_exists($mod->modname.'reopened', 'block_assignment_overview')) {
            $meta->reopenedstr = get_string($mod->modname.'reopened', 'block_assignment_overview');
        } else {
            $meta->reopenedstr = get_string('reopened', 'block_assignment_overview');
        }

        // If module is not visible to the user then don't bother getting meta data.
        if (!$mod->uservisible) {
            return $meta;
        }

        $meta->timeopen = $tiitwodata['dtstart'];
        $meta->timeclose = $tiitwodata['dtdue'];

        // If role has specific "teacher" capabilities.
        if (has_capability('mod/assign:grade', $mod->context)) {
            $meta->isteacher = true;

            // Teacher - useful teacher meta data.
            $meta->numsubmissions = $tiitwodata['numsubmissions'];
            $meta->numrequiregrading = $tiitwodata['totalusers'] - $tiitwodata['graded'];

            if (!empty($tiitwodata['submissioninfo'])) {
                $meta->submissioninfo = $tiitwodata['submissioninfo'];
            }

        } else {
            // Student - useful student meta data - only display if activity is available.
            if (empty($activitydates->timeopen) || $activitydates->timeopen <= time()) {

                $submissionrow = '';

                $meta->submitted = $tiitwodata['submitted'];
                if (!empty($meta->submitted)) {
                    $meta->timesubmitted = $tiitwodata['modified'];
                }

                if (!empty($tiitwodata['submissioninfo'])) {
                    $meta->submissioninfo = $tiitwodata['submissioninfo'];
                }
            }
        }

        // We assume this is always gradeable.
        $isgradeable = true;

        if (isset($config->furtherinformationincludesubmissionfeedback)
                && $config->furtherinformationincludesubmissionfeedback == true) {
            $graderow = false;
            if ($isgradeable) {
                $graderow = self::grade_row($courseid, $mod);
            }

            if ($graderow) {
                $gradeitem = \grade_item::fetch(array(
                        'itemtype' => 'mod',
                        'itemmodule' => $mod->modname,
                        'iteminstance' => $mod->instance,
                ));

                $grade = new \grade_grade(array('itemid' => $gradeitem->id, 'userid' => $USER->id));

                $coursecontext = \context_course::instance($courseid);
                $canviewhiddengrade = has_capability('moodle/grade:viewhidden', $coursecontext);

                if (!$grade->is_hidden() || $canviewhiddengrade) {
                    $meta->grade = true;
                }
            }
        }

        return $meta;
    }

    /**
     * Get assignment meta data
     *
     * @param \cm_info $modinst - module instance
     * @return activity_meta
     */
    public static function assign_meta(\cm_info $modinst) {
        global $DB;
        $courseid = $modinst->course;
        static $submissionsenabled = array();

        // Get count of enabled submission plugins grouped by assignment id.
        // Note, under normal circumstances we only run this once but with PHP unit tests, assignments are being
        // created one after the other and so this needs to be run each time during a PHP unit test.
        if (empty($submissionsenabled[$courseid])) {
            $sql = "SELECT a.id, count(1) AS submissionsenabled
                      FROM {assign} a
                      JOIN {assign_plugin_config} ac ON ac.assignment = a.id
                     WHERE a.course = ?
                       AND ac.name='enabled'
                       AND ac.value = '1'
                       AND ac.subtype='assignsubmission'
                       AND plugin!='comments'
                  GROUP BY a.id;";
            $submissionsenabled[$courseid] = $DB->get_records_sql($sql, array($courseid));
        }

        $submitselect = '';

        // If there aren't any submission plugins enabled for this module, then submissions are not required.
        if (empty($submissionsenabled[$courseid][$modinst->instance])) {
            $submissionnotrequired[$courseid] = true;
        } else {
            $submissionnotrequired[$courseid] = false;
        }

        $meta = self::std_meta($modinst, 'allowsubmissionsfromdate', 'duedate', 'assignment', 'submission',
                'timemodified', 'submitted', true, $submitselect, $submissionnotrequired[$courseid]);

        return ($meta);
    }


    /**
     * Get all assignments (for all courses) waiting to be graded.
     *
     * @param array $courseids
     * @param int $since
     * @return array $ungraded
     */
    public static function assign_ungraded($courseids, $since = null) {
        global $DB;

        $ungraded = array();

        if ($since === null) {
            $since = time() - (12 * WEEKSECS);
        }

        // Limit to assignments with grades.
        $gradetypelimit = 'AND gi.gradetype NOT IN (' . GRADE_TYPE_NONE . ',' . GRADE_TYPE_TEXT . ')';

        foreach ($courseids as $courseid) {

            // Get the assignments that need grading.
            list($esql, $params) = get_enrolled_sql(\context_course::instance($courseid), 'mod/assign:submit', 0, true);
            $params['courseid'] = $courseid;

            $sql = "-- Snap sql
            SELECT cm.id AS coursemoduleid, a.id AS instanceid, a.course,
            a.allowsubmissionsfromdate AS opentime, a.duedate AS closetime,
            count(DISTINCT sb.userid) AS ungraded
            FROM {assign} a
            JOIN {course} c ON c.id = a.course
            JOIN {modules} m ON m.name = 'assign'

            JOIN {course_modules} cm
            ON cm.module = m.id
            AND cm.instance = a.id

            JOIN {assign_submission} sb
            ON sb.assignment = a.id
            AND sb.latest = 1

            JOIN ($esql) e
            ON e.id = sb.userid

            -- Start of join required to make assignments marked via gradebook not show as requiring grading
            -- Note: This will lead to disparity between the assignment page (mod/assign/view.php[questionmark]id=[id])
            -- and the module page will still say that 1 item requires grading.

            LEFT JOIN {assign_grades} ag
            ON ag.assignment = sb.assignment
            AND ag.userid = sb.userid
            AND ag.attemptnumber = sb.attemptnumber

            LEFT JOIN {grade_items} gi
            ON gi.courseid = a.course
            AND gi.itemtype = 'mod'
            AND gi.itemmodule = 'assign'
            AND gi.itemnumber = 0
            AND gi.iteminstance = cm.instance

            LEFT JOIN {grade_grades} gg
            ON gg.itemid = gi.id
            AND gg.userid = sb.userid

            -- End of join required to make assignments classed as graded when done via gradebook

            WHERE sb.status = 'submitted'
            AND a.course = :courseid

            AND (
            sb.timemodified > gg.timemodified
            OR gg.finalgrade IS NULL
            )

            AND (a.duedate = 0 OR a.duedate > $since)
            $gradetypelimit
            GROUP BY instanceid, a.course, opentime, closetime, coursemoduleid ORDER BY a.duedate ASC";
            $rs = $DB->get_records_sql($sql, $params);
            $ungraded = array_merge($ungraded, $rs);
        }

        return $ungraded;
    }

    /**
     * Get number of ungraded submissions for specific assignment
     * Based on count_submissions_need_grading() in mod/assign/locallib.php
     *
     * @param int $courseid
     * @param int $modid
     * @return int
     */
    public static function assign_num_submissions_ungraded($courseid, $modid) {
        global $DB;

        static $hasgrades = null;
        static $totalsbyid;

        // Use cache to see if assign has grades.
        if ($hasgrades != null && !isset($hasgrades[$modid])) {
            return 0;
        }

        // Use cache to return number of assigns yet to be graded.
        if (!empty($totalsbyid)) {
            if (isset($totalsbyid[$modid])) {
                return intval($totalsbyid[$modid]->total);
            } else {
                return 0;
            }
        }

        // Check to see if this assign is graded.
        $params = array(
                'courseid'      => $courseid,
                'itemtype'      => 'mod',
                'itemmodule'    => 'assign',
                'gradetypenone' => GRADE_TYPE_NONE,
                'gradetypetext' => GRADE_TYPE_TEXT,
        );

        $sql = 'SELECT iteminstance
                FROM {grade_items}
                WHERE courseid = ?
                AND itemtype = ?
                AND itemmodule = ?
                AND gradetype <> ?
                AND gradetype <> ?';

        $hasgrades = $DB->get_records_sql($sql, $params);

        if (!isset($hasgrades[$modid])) {
            return 0;
        }

        // Get grading information for remaining of assigns.
        $coursecontext = \context_course::instance($courseid);
        list($esql, $params) = get_enrolled_sql($coursecontext, 'mod/assign:submit', 0, true);

        $params['submitted'] = ASSIGN_SUBMISSION_STATUS_SUBMITTED;
        $params['courseid'] = $courseid;

        $sql = "-- Snap sql
        SELECT sb.assignment, count(sb.userid) AS total
        FROM {assign_submission} sb

        JOIN {assign} an
        ON sb.assignment = an.id

        LEFT JOIN {assign_grades} ag
        ON sb.assignment = ag.assignment
        AND sb.userid = ag.userid
        AND sb.attemptnumber = ag.attemptnumber

        -- Start of join required to make assignments marked via gradebook not show as requiring grading
        -- Note: This will lead to disparity between the assignment page (mod/assign/view.php[questionmark]id=[id])
        -- and the module page will still say that 1 item requires grading.

        LEFT JOIN {grade_items} gi
        ON gi.courseid = an.course
        AND gi.itemtype = 'mod'
        AND gi.itemmodule = 'assign'
        AND gi.itemnumber = 0
        AND gi.iteminstance = an.id

        LEFT JOIN {grade_grades} gg
        ON gg.itemid = gi.id
        AND gg.userid = sb.userid

        -- End of join required to make assignments classed as graded when done via gradebook

        -- Start of enrolment join to make sure we only include students that are allowed to submit. Note this causes an ALL
        -- join on mysql!
        JOIN ($esql) e
        ON e.id = sb.userid
        -- End of enrolment join

        WHERE an.course = :courseid
        AND sb.timemodified IS NOT NULL
        AND sb.status = :submitted
        AND sb.latest = 1

        AND (
        sb.timemodified > gg.timemodified
        OR gg.finalgrade IS NULL
        )

        GROUP BY sb.assignment
        ";

        $totalsbyid = $DB->get_records_sql($sql, $params);
        return isset($totalsbyid[$modid]) ? intval($totalsbyid[$modid]->total) : 0;
    }

    /**
     * Assign module function for getting number of submissions
     *
     * @param int $courseid
     * @param int $modid
     * @return int
     */
    public static function assign_num_submissions($courseid, $modid) {
        global $DB;

        static $modtotalsbyid = array();

        if (!isset($modtotalsbyid['assign'][$courseid])) {
            // Results are not cached, so lets get them.

            list($esql, $params) = get_enrolled_sql(\context_course::instance($courseid), 'mod/assign:submit', 0, true);
            $params['courseid'] = $courseid;
            $params['submitted'] = ASSIGN_SUBMISSION_STATUS_SUBMITTED;

            // Get the number of submissions for all assign activities in this course.
            $sql = "-- Snap sql
            SELECT m.id, COUNT(sb.userid) as totalsubmitted
            FROM {assign} m
            JOIN {assign_submission} sb
            ON m.id = sb.assignment
            AND sb.latest = 1

            JOIN ($esql) e
            ON e.id = sb.userid

            WHERE m.course = :courseid
            AND sb.status = :submitted
            GROUP by m.id";
            $modtotalsbyid['assign'][$courseid] = $DB->get_records_sql($sql, $params);
        }
        $totalsbyid = $modtotalsbyid['assign'][$courseid];

        if (!empty($totalsbyid)) {
            if (isset($totalsbyid[$modid])) {
                return intval($totalsbyid[$modid]->totalsubmitted);
            }
        }
        return 0;
    }

    /**
     * Data module function for getting number of contributions
     *
     * @param int $courseid
     * @param int $modid
     * @return int
     */
    public static function data_num_contributions($courseid, $modid) {
        global $DB;

        static $modtotalsbyid = array();

        if (!isset($modtotalsbyid['data'][$modid])) {
            $params['dataid'] = $modid;

            // Get the number of contributions for this data activity.
            $sql = '
             SELECT d.id, count(dataid) as total FROM {data_records} r, {data} d
                WHERE r.dataid = d.id AND r.dataid = :dataid';

            $modtotalsbyid['data'][$modid] = $DB->get_records_sql($sql, $params);
        }
        $totalsbyid = $modtotalsbyid['data'][$modid];

        if (!empty($totalsbyid)) {
            if (isset($totalsbyid[$modid])) {
                return intval($totalsbyid[$modid]->total);
            }
        }
        return 0;
    }

    /**
     * Get activity submission row
     *
     * @param int      $courseid
     * @param \cm_info $mod
     * @param string   $submissiontable
     * @param string   $modfield
     * @param string   $extraselect
     * @return mixed
     */
    public static function get_submission_row($courseid, $mod, $submissiontable, $modfield, $extraselect='') {
        global $DB, $USER;

        // Note: Caches all submissions to minimise database transactions.
        static $submissions = array();

        // Pull from cache?
        if (!PHPUNIT_TEST) {
            if (isset($submissions[$courseid.'_'.$mod->modname])) {
                if (isset($submissions[$courseid.'_'.$mod->modname][$mod->instance])) {
                    return $submissions[$courseid.'_'.$mod->modname][$mod->instance];
                } else {
                    return false;
                }
            }
        }

        $submissiontable = $mod->modname.'_'.$submissiontable;

        if ($mod->modname === 'assign') {
            $params = [$courseid, $USER->id];
            $sql = "-- Snap sql
                SELECT a.id AS instanceid, st.*
                    FROM {".$submissiontable."} st

                    JOIN {".$mod->modname."} a
                    ON a.id = st.$modfield

                    WHERE a.course = ?
                    AND st.latest = 1
                    AND st.userid = ? $extraselect
                    ORDER BY $modfield DESC, st.id DESC";
        } else {
            // Less effecient general purpose for other module types.
            $params = [$USER->id, $courseid, $USER->id];
            $sql = "-- Snap sql
                SELECT a.id AS instanceid, st.*
                    FROM {".$submissiontable."} st

                    JOIN {".$mod->modname."} a
                    ON a.id = st.$modfield

                    -- Get only the most recent submission.
                    JOIN (SELECT $modfield AS modid, MAX(id) AS maxattempt
                    FROM {".$submissiontable."}
                    WHERE userid = ?
                    GROUP BY modid) AS smx
                    ON smx.modid = st.$modfield
                    AND smx.maxattempt = st.id

                    WHERE a.course = ?
                    AND st.userid = ? $extraselect
                    ORDER BY $modfield DESC, st.id DESC";
        }

        // Not every activity has a status field...
        // Add one if it is missing so code assuming there is a status property doesn't explode.
        $result = $DB->get_records_sql($sql, $params);
        if (!$result) {
            unset($submissions[$courseid.'_'.$mod->modname]);
            return false;
        }

        foreach ($result as $r) {
            if (!isset($r->status)) {
                $r->status = null;
            }
        }

        $submissions[$courseid.'_'.$mod->modname] = $result;

        if (isset($submissions[$courseid.'_'.$mod->modname][$mod->instance])) {
            return $submissions[$courseid.'_'.$mod->modname][$mod->instance];
        } else {
            return false;
        }
    }

    /**
     * Get the activity dates for a specific module instance
     *
     * @param int      $courseid
     * @param stdClass $mod
     * @param string   $timeopenfld
     * @param string   $timeclosefld
     *
     * @return bool|stdClass
     */
    public static function instance_activity_dates($courseid, $mod, $timeopenfld = '', $timeclosefld = '') {
        global $DB, $USER;

        // Note: Caches all moduledates to minimise database transactions.
        static $moddates = array();
        if (!isset($moddates[$courseid . '_' . $mod->modname][$mod->instance]) || PHPUNIT_TEST) {
            $timeopenfld = $mod->modname === 'quiz' ? 'timeopen' : ($mod->modname === 'lesson' ? 'available' : $timeopenfld);
            $timeclosefld = $mod->modname === 'quiz' ? 'timeclose' : ($mod->modname === 'lesson' ? 'deadline' : $timeclosefld);
            $sql = "-- Snap sql
            SELECT
            module.id,
            module.$timeopenfld AS timeopen,
            module.$timeclosefld AS timeclose";
            if ($mod->modname === 'assign') {
                $sql .= ",
                    auf.extensionduedate AS extension
                ";
            }
            if ($mod->modname === 'quiz' || $mod->modname === 'lesson') {
                $id = $mod->modname === 'quiz' ? $mod->modname : 'lessonid';
                $groups = groups_get_user_groups($courseid);
                $groupbysql = '';
                $params = array();
                if ($groups[0]) {
                    list ($groupsql, $params) = $DB->get_in_or_equal($groups[0]);
                    if ($DB->get_dbfamily() === 'mysql') {
                        $sql .= ",
                        CASE
                        WHEN ovrd1.$timeopenfld IS NULL
                        THEN MIN(ovrd2.$timeopenfld)
                        ELSE ovrd1.$timeopenfld
                        END AS timeopenover,
                        CASE
                        WHEN ovrd1.$timeclosefld IS NULL
                        THEN MAX(ovrd2.$timeclosefld)
                        ELSE ovrd1.$timeclosefld
                        END AS timecloseover
                        FROM {" . $mod->modname . "} module";
                    } else {
                        $sql .= ",
                        MIN (
                        CASE
                        WHEN ovrd1.$timeopenfld IS NULL
                        THEN ovrd2.$timeopenfld
                        ELSE ovrd1.$timeopenfld
                        END
                        ) AS timeopenover,
                        MAX (
                        CASE
                        WHEN ovrd1.$timeclosefld IS NULL
                        THEN ovrd2.$timeclosefld
                        ELSE ovrd1.$timeclosefld
                        END
                        ) AS timecloseover
                        FROM {" . $mod->modname . "} module";
                    }
                    array_unshift($params, $USER->id); // Add userid to start of params.
                    $sql .= "
                        LEFT JOIN {" . $mod->modname . "_overrides} ovrd1
                        ON module.id=ovrd1.$id
                        AND ovrd1.userid = ?
                        LEFT JOIN {" . $mod->modname . "_overrides} ovrd2
                        ON module.id=ovrd2.$id
                        AND ovrd2.groupid $groupsql";
                    $groupbysql = "
                    GROUP BY module.id, module.$timeopenfld, module.$timeclosefld";

                } else {
                    $params[] = $USER->id;
                    $sql .= ", ovrd1.$timeopenfld AS timeopenover, ovrd1.$timeclosefld AS timecloseover
                    FROM {" . $mod->modname . "} module
                             LEFT JOIN {" . $mod->modname . "_overrides} ovrd1
                             ON module.id=ovrd1.$id AND ovrd1.userid = ?";
                }
                $sql .= " WHERE module.course = ?";
                $sql .= $groupbysql;
                $params[] = $courseid;
                $result = $DB->get_records_sql($sql, $params);
            } else {
                $params = [];
                $sql .= "  FROM {" . $mod->modname . "} module";
                if ($mod->modname === 'assign') {
                    $params[] = $USER->id;
                    $sql .= "
                      LEFT JOIN {assign_user_flags} auf
                             ON module.id = auf.assignment
                            AND auf.userid = ?
                     ";
                }
                $params[] = $courseid;
                $sql .= " WHERE module.course = ?";
                $result = $DB->get_records_sql($sql, $params);
            }
            $moddates[$courseid . '_' . $mod->modname] = $result;
        }
        $modinst = $moddates[$courseid.'_'.$mod->modname][$mod->instance];
        if (!empty($modinst->timecloseover)) {
            $modinst->timeclose = $modinst->timecloseover;
            if ($modinst->timeopenover) {
                $modinst->timeopen = $modinst->timeopenover;
            }
        }
        return $modinst;

    }

    /**
     * Return grade row for specific module instance.
     *
     * @param int      $courseid
     * @param \cm_info $mod
     * @return bool
     */
    public static function grade_row($courseid, $mod) {
        global $DB, $USER;

        static $grades = array();

        if (isset($grades[$courseid.'_'.$mod->modname])
            && isset($grades[$courseid.'_'.$mod->modname][$mod->instance])
            ) {
                return $grades[$courseid.'_'.$mod->modname][$mod->instance];
        }

        $sql = "-- Snap sql
        SELECT m.id AS instanceid, gg.*

            FROM {".$mod->modname."} m

            JOIN {grade_items} gi
              ON m.id = gi.iteminstance
             AND gi.itemtype = 'mod'
             AND gi.itemmodule = :modname
             AND gi.courseid = :courseid1

            JOIN {grade_grades} gg
              ON gi.id = gg.itemid

           WHERE m.course = :courseid2
             AND gg.userid = :userid
             AND (
                 gg.rawgrade IS NOT NULL
                 OR gg.finalgrade IS NOT NULL
                 OR gg.feedback IS NOT NULL
             )
             ";
        $params = array(
                'modname' => $mod->modname,
                'courseid1' => $courseid,
                'courseid2' => $courseid,
                'userid' => $USER->id
        );
        $grades[$courseid.'_'.$mod->modname] = $DB->get_records_sql($sql, $params);

        if (isset($grades[$courseid.'_'.$mod->modname][$mod->instance])) {
            return $grades[$courseid.'_'.$mod->modname][$mod->instance];
        } else {
            return false;
        }
    }

    /**
     * Get everything graded from a specific date to the current date.
     *
     * @param bool $onlyactive - only show grades in courses actively enrolled on if true.
     * @param null|int $showfrom - timestamp to show grades from. Note if not set defaults to 1 month ago.
     * @return mixed
     */
    public static function events_graded($onlyactive = true, $showfrom = null) {
        global $DB, $USER;

        $params = [];
        $coursesql = '';
        if ($onlyactive) {
            $courses = enrol_get_my_courses();
            $courseids = array_keys($courses);
            $courseids[] = SITEID;
            list ($coursesql, $params) = $DB->get_in_or_equal($courseids);
            $coursesql = 'AND gi.courseid '.$coursesql;
        }

        $onemonthago = time() - (DAYSECS * 31);
        $showfrom = $showfrom !== null ? $showfrom : $onemonthago;

        $sql = "-- Snap sql
        SELECT gg.*, gi.itemmodule, gi.iteminstance, gi.courseid, gi.itemtype
        FROM {grade_grades} gg
        JOIN {grade_items} gi
        ON gg.itemid = gi.id $coursesql
        WHERE gg.userid = ?
        AND (gg.timemodified > ?
        OR gg.timecreated > ?)
        AND (gg.finalgrade IS NOT NULL
        OR gg.rawgrade IS NOT NULL
        OR gg.feedback IS NOT NULL)
        AND gi.itemtype = 'mod'
        ORDER BY timemodified DESC";

        $params = array_merge($params, [$USER->id, $showfrom, $showfrom]);
        $grades = $DB->get_records_sql($sql, $params, 0, 5);

        $eventdata = array();
        foreach ($grades as $grade) {
            $eventdata[] = $grade;
        }

        return $eventdata;
    }

    /**
     * Return extension date for user on assignment.
     * @param int $assignmentid
     * @return int | bool
     */
    public static function assignment_user_extension_date($assignmentid) {
        global $USER, $DB;
        $vars = array('assignment' => $assignmentid, 'userid' => $USER->id);
        $row = $DB->get_record('assign_user_flags', $vars, 'extensionduedate');
        return $row ? $row->extensionduedate : false;
    }

    /**
     * Get the module meta data for a specific module. This is generic code
     * to cater for multiple types of mods but really we're only interested in assignment mods.
     *
     * @param cm_info $mod
     * @param int     $course
     * @param array   $tiitwodata Optional data from TII two. If included, this already has some of the data required
     * @return array
     */
    public static function course_section_cm_get_data(cm_info $mod, $course, $tiitwodata = array()) {

        global $OUTPUT, $CFG;

        $courseid = $mod->course;
        $content = '';

        $returndata = array();

        $config = get_config("block_assignment_overview");

        if (is_guest(context_course::instance($courseid))) {
            return $returndata;
        }

        // First check if any _meta method exists for this
        // mod.  This is just so we can return an icon
        // for it initially if it does exist and ignore
        // less important mods (e.g. without a due date for example).
        $methodname = $mod->modname . '_meta';
        if (!method_exists('block_assignment_overview\\activity', $methodname)) {
            // Not going to display meta data for this module.
            return $returndata;
        }

        $modurl = new \moodle_url("/mod/{$mod->modname}/view.php", ['id' => $mod->id]);

        if (!empty ($tiitwodata)) {
            $returndata['name'] = $tiitwodata['name'];
        } else {
            $returndata['name'] = $mod->name;
        }
        $returndata['url'] = $modurl;
        $returndata['modname'] = $mod->modname;

        // Now get actual meta data for module as code has passed the checks.
        $meta = self::module_meta($mod, $tiitwodata);
        if (!$meta->is_set(true)) {
            // Can't get meta data for this module.
            return $returndata;
        }

        // Set $datefield which is used further down as the due date.
        $datefield = '';

        if (!empty($meta->extension)) {
            $datefield = 'extension';
        } else if (!empty($meta->timeclose)) {
            $datefield = 'timeclose';
        }

        $warningclass = '';
        if ($meta->submitted) {
            $warningclass = ' block-assignments-overview-activity-date-submitted ';
        }

        $datebuttondisplaytext = '';

        // Get the submitted date if submitted or not submitted text.
        $submittedtext = '';
        $notsubmittedtext = '';
        $displayfurtherinfo = true;

        $activitycontent = self::submission_cta($mod, $meta);
        if (!(empty($activitycontent))) {

            // Format submitted date in button for assignments.
            if ($meta->submitted) {
                $submittedtext .= html_writer::start_tag('span', array('class' => ' block-assignments-overview-activity-due-date '
                        . $warningclass));
                $submittedtext .= $activitycontent;
                $submittedtext .= html_writer::end_tag('span');
            } else {
                // Only display if this is really a student on the course (i.e. not anyone who can grade an assignment).
                if (!has_capability('mod/assign:grade', $mod->context)) {
                    $submittedtext .= html_writer::start_tag('div');
                    $submittedtext .= $activitycontent;
                    $submittedtext .= html_writer::end_tag('div');
                }
            }

            $returndata['submittedstatus'] = $submittedtext;
        }

        // -------------------------------
        // Get due date or date submitted.
        // -------------------------------
        if (!empty($datefield)) {

            $returndata['duedate'] = $meta->$datefield;

            // Create formatted due date.
            $dateformat = get_string('strftimedate', 'langconfig');
            $formatteduedate = get_string('due', 'block_assignment_overview', userdate($meta->$datefield, $dateformat));

            $pastdue = $meta->$datefield < time();

            // Create URL for due date.
            $url = new \moodle_url("/mod/{$mod->modname}/view.php", ['id' => $mod->id]);

            // Display assignment status (due, nearly due, overdue), as long as it hasn't been submitted,
            // or submission not required.
            if ( (!$meta->submitted) && (!$meta->submissionnotrequired)) {
                $warningclass = '';
                $labeltext = '';

                // If assignment due in 7 days or less, display in amber, if overdue, then in red, or if submitted, turn to green.

                // If assignment is 7 days before date due(nearly due).
                $timedue = $meta->$datefield - (86400 * 7);
                if ( (time() > $timedue) &&  !(time() > $meta->$datefield) ) {
                    $warningclass = ' block-assignments-overview-activity-date-nearly-due';
                    $returndata['displaytype'] = 'assignmentsdue';
                } else if (time() > $meta->$datefield) { // If assignment is actually overdue.

                    // Now check threshold for displaying this.
                    $warningclass = ' block-assignments-overview-activity-date-overdue';
                    $labeltext .= html_writer::tag('i', '&nbsp;', array('class' => 'fa fa-exclamation')) . ' ';
                }

                $labeltext .= $formatteduedate;

                $activityclass = ' block-assignments-overview-activity-due-date ';

                $datebuttondisplaytext .= html_writer::start_tag('span', array('class' => $activityclass . $warningclass));
                $datebuttondisplaytext .= html_writer::link($url, $labeltext);
                $datebuttondisplaytext .= html_writer::end_tag('span');
            } else {
                if ($submittedtext) {
                    $datebuttondisplaytext .= $submittedtext . '<br>';
                }
                $datebuttondisplaytext .= $formatteduedate;
            }
            $returndata['formattedstatus'] = $datebuttondisplaytext;

        } else {
            if ($submittedtext) {
                $datebuttondisplaytext .= $submittedtext . '<br>';

            } else {
                // No due date but display submitted status if available or display no due date.
                $datebuttondisplaytext = 'No Due Date';
            }

            $returndata['duedate'] = '';
            $returndata['formattedstatus'] = $datebuttondisplaytext;
        }

        $content .= html_writer::start_tag('div', array('class' => 'assignment-overview-activity-mod-engagement'));
        $content .= $datebuttondisplaytext . html_writer::end_tag('div');

        $content .= html_writer::end_tag('div');

        // ---------------------------------------------------------------
        // Column 2.  Contains not submitted status or feedback available.
        // ---------------------------------------------------------------
        $content .= html_writer::start_tag('div', array('class' => 'col-5 my-auto'));

        if (isset($config->furtherinformationincludesubmissionfeedback)
                && $config->furtherinformationincludesubmissionfeedback == true) {

            if ($meta->isteacher) {
                // Teacher - useful teacher meta data.
                $engagementmeta = array();

                // Below, !== false means we get 0 out of x submissions.
                if (!$meta->submissionnotrequired && $meta->numsubmissions !== false) {
                    $engagementmeta[] = get_string('xofysubmitted', 'block_assignment_overview',
                            (object) array(
                                    'completed' => $meta->numsubmissions,
                                    'participants' => \block_assignment_overview\utils::course_participant_count(
                                            $courseid, $mod->modname)
                            )
                            );
                }

                if ($meta->numrequiregrading) {
                    $engagementmeta[] = get_string('xungraded', 'block_assignment_overview', $meta->numrequiregrading);
                }
                if (!empty($engagementmeta)) {
                    $engagementstr = implode(', ', $engagementmeta);

                    $params = array(
                            'action' => 'grading',
                            'id' => $mod->id,
                            'tsort' => 'timesubmitted',
                            'filter' => 'require_grading'
                    );
                    $url = new moodle_url("/mod/{$mod->modname}/view.php", $params);

                    $icon = html_writer::tag('i', '&nbsp;', array('class' => 'fa fa-info-circle'));
                    $returndata['formattedfurtherinfo'] = html_writer::start_tag('div',
                                                            array('class' => 'assignment-overview-activity-mod-engagement'));
                    $returndata['formattedfurtherinfo'] .= html_writer::link($url, $icon . $engagementstr);
                    $returndata['formattedfurtherinfo'] .= html_writer::end_tag('div');
                    if (!empty ($meta->submissioninfo)) {
                        $returndata['formattedfurtherinfo'] .= '<br>' . $meta->submissioninfo;
                    }
                }

            } else {
                $feedbacktext = '';
                // Check if feedback available (actually this checks if grade is available).
                if (!empty($meta->grade)) {
                    $url = new \moodle_url('/grade/report/user/index.php', ['id' => $courseid]);
                    if (in_array($mod->modname, ['turnitintooltwo', 'assign'])) {
                        $url = new \moodle_url('/mod/'.$mod->modname.'/view.php?id='.$mod->id);
                    }

                    $feedbacktext .= html_writer::tag('i', '&nbsp;', array('class' => 'fa fa-commenting-o'));
                    $feedbacktext .= html_writer::link($url, get_string('gradeavailable', 'block_assignment_overview'));

                } else {
                    if ($meta->submissionnotrequired) {
                        $feedbacktext .= get_string('submissionnotrequired', 'block_assignment_overview') . '<br>';
                    } else {
                        // If submitted and no grade available.
                        if ($meta->submitted) {
                            $feedbacktext .= get_string('gradenotavailable', 'block_assignment_overview');
                        } else {
                            if (!empty ($meta->submissioninfo)) {
                                $feedbacktext .= $meta->submissioninfo;
                            } else {
                                $feedbacktext .= get_string('notsubmitted', 'block_assignment_overview');
                            }
                        }
                    }

                }

                $returndata['formattedfurtherinfo'] = html_writer::start_tag('span',
                            array('class' => 'assignment-overview-activity-mod-feedback'));
                $returndata['formattedfurtherinfo'] .= $feedbacktext;
                $returndata['formattedfurtherinfo'] .= html_writer::end_tag('span');

                // If submissions are not allowed yet, return the date it is allowed and add format this.
                if (!empty($meta->timeopen) && $meta->timeopen > time()) {
                    $content .= html_writer::start_tag('span', array('class' => 'assignment-overview-activity-mod-feedback'));
                    $icon = html_writer::tag('i', '&nbsp;', array('class' => 'fa fa-info-circle'));
                    $dateformat = get_string('strftimedate', 'langconfig');

                    $returndata['formattedfurtherinfo'] .= '<br>' . $icon . ' Submissions allowed from '
                                                            . userdate($meta->timeopen, $dateformat);
                    $returndata['formattedfurtherinfo'] .= html_writer::end_tag('span');
                }
            }

        }

        // -------------------------------------------------------------------------------------------
        // Work out if this is an overdue, past assignment submitted or past assignment not submitted.
        // -------------------------------------------------------------------------------------------

        $timenow = time();

        // Assignments due threshold (in days). This determines which assignments are shown on front end to be, due up to a
        // maximum of days overdue. E.g. Assignments no older than this number of days that are overdue.
        $assignmentsduedefaultthreshold = 7;
        if (!empty($config->assignmentsduethreshold)) {
            $assignmentsduethreshold = $config->assignmentsduethreshold * 86400;
        } else {
            $assignmentsduethreshold = $assignmentsduedefaultthreshold * 86400;
        }

        $maxoverduedate = $timenow - $assignmentsduethreshold;

        if ($meta->isteacher) {
            if ( (empty($returndata['duedate'])) ) {
                $returndata['displaytype'] = 'assignmentsnoduedate';
            } else if ( ($timenow < $returndata['duedate'] + $assignmentsduethreshold)  ) {
                $returndata['displaytype'] = 'assignmentsdue';
            } else if ($timenow > $returndata['duedate'] ) {
                $returndata['displaytype'] = 'pastassignmentsnotsubmitted';
            } else if (($meta->numsubmissions > 0)  && ($timenow > $returndata['duedate'])
                        && ($returndata['duedate'] > $maxoverduedate) ) {
                $returndata['displaytype'] = 'pastassignmentssubmitted';
            }
        } else {
            if ( (!$meta->submitted) && (empty($returndata['duedate'])) ) {
                $returndata['displaytype'] = 'assignmentsnoduedate';
            } else if ( (!$meta->submitted) && ($timenow < $returndata['duedate'] + $assignmentsduethreshold) ) {
                $returndata['displaytype'] = 'assignmentsdue';
            } else if ( (!$meta->submitted) && ($timenow > $returndata['duedate']) ) {
                $returndata['displaytype'] = 'pastassignmentsnotsubmitted';
            } else if (($meta->submitted)  ) {
                $returndata['displaytype'] = 'pastassignmentssubmitted';
            }

        }

        $meta = null;

        return $returndata;
    }

    /**
     * Submission call to action.
     *
     * @param cm_info $mod
     * @param activity_meta $meta
     * @return string
     * @throws coding_exception
     */
    public static function submission_cta(cm_info $mod, \block_assignment_overview\activity_meta $meta) {
        global $CFG;

        if (empty($meta->submissionnotrequired)) {

            $url = $CFG->wwwroot.'/mod/'.$mod->modname.'/view.php?id='.$mod->id;

            if ($meta->submitted) {
                if (empty($meta->timesubmitted)) {
                    $submittedonstr = '';
                } else {
                    $submittedonstr = ' '.userdate($meta->timesubmitted, get_string('strftimedate', 'langconfig'));
                }
                $message = html_writer::tag('i', '&nbsp;', array('class' => 'fa fa-check')) . $meta->submittedstr.$submittedonstr;
            } else {
                $warningstr = $meta->draft ? $meta->draftstr : $meta->notsubmittedstr;
                $warningstr = $meta->reopened ? $meta->reopenedstr : $warningstr;
                $message = $warningstr;
                $message = html_writer::tag('i', '&nbsp;', array('class' => 'fa fa-info-circle')) . $message;
            }

            return html_writer::link($url, $message);
        }
        return '';
    }

}
