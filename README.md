# Moodle Assignment Overview #

This block displays assignment and their status (including Turnitintooltwo assignments) for the currently logged in user, on a frontpage or dashboard page. These are filtered by the following:

- Assignments due
- Assignments submitted
- Past assignments not submitted
- Assignments with no due date

There is a toggle available for the above filters just above the display of assignments.

# Guidelines for use #

The block should be added to a block region that will appear on the frontpage or dashboard page.

## Other points to note when using this plugin ##

This plugin has been designed to work with the Adaptable theme but it should work with most other themes.

Version 1.3.4 (2020030900)

### How do I get set up? ###

Installs at /blocks/assignment_overview

## Site-wide settings ##

Site-wide configuration options are available under:  Site Administration > Plugins > Blocks > Assignment Overview

### Use caching ###
Default: No
Toggle for caching (uses Moodle cache API). Strongly recommended for better performance! Particularly if you have a larger site with large volmes of students, courses and activities.

### Cache expiry time ###
Default: 300
Cache expiry time in seconds (TTL).

### Assignment title text ###
Default: Empty
Any text you want to appear at the top of the block. E.g. "Your Assignments". 

### Max height with scrollbar ###
Default: No
Select this to have the content a maximum height, with a scrollbar where needed. This is useful in the case of a long list of assignments, to avoid the content extending the height of the page excessively. The max height is fixed in the CSS code. This is done in a CSS class called "block-assignments-overview-container-scrollable" and could be overridden in many themes if a different max height is desired.

### Include Submission / Feedback information ###
Default: Noe
Include Submission / Feedback information for teachers and students.

### Assignments due threshold ###
Default: 7
Display assignments due that are no older (in days) than this value.

### Include Turnitin Tool Two assignments ###
Also show Turnitin Tool Two plugin  assignments in the list of assignments. This refers to this activity: https://moodle.org/plugins/mod_turnitintooltwo.

### Default value for main filter ###
Default value for main filter when no user preference is set. This is useful if you want users to see a particular default filter before one is set.

## Instance settings ##

None at present.

## Compatibility ##

- Moodle 3.6, 3.7, 3.8

## Contribution ##

Developed by:

 * Manoj Solanki (Coventry University)

Co-maintained by:

 * Jeremy Hopkins (Coventry University)

 ## Licenses ##

Licensed under: GPL v3 (GNU General Public License) - http://www.gnu.org/licenses
