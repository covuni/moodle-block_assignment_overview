<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Assignment Overview block helper functions and callbacks.
 *
 * @package block_assignment_overview
 * @copyright 2019 Manoj Solanki (Coventry University)
 *
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */
defined('MOODLE_INTERNAL') || die();

define( 'ASSIGNMENTS_OVERVIEW_CACHE_TTL_DEFAULT', 300);
define( 'ASSIGNMENTS_OVERVIEW_THRESHOLD_ASSIGNMENTS_DUE_DEFAULT', 7);

/**
 * Constants for the user preferences filter options
 */
define('BLOCK_ASSIGNMENTS_OVERVIEW_ASSIGNMENTS_DUE', 'assignmentsdue');
define('BLOCK_ASSIGNMENTS_OVERVIEW_PAST_ASSIGNMENTS_SUBMITTED', 'pastassignmentssubmitted');
define('BLOCK_ASSIGNMENTS_OVERVIEW_PAST_ASSIGNMENTS_NOT_SUBMITTED', 'pastassignmentsnotsubmitted');
define('BLOCK_ASSIGNMENTS_OVERVIEW_ASSIGNMENTS_NO_DUE_DATE', 'assignmentsnoduedate');
define('BLOCK_ASSIGNMENTS_OVERVIEW_ASSIGNMENTS_ALL', 'assignmentsall');

/**
 * Get the current user preferences that are available
 *
 * @return mixed Array representing current options along with defaults
 */
function block_assignment_overview_user_preferences() {

    $preferences['block_assignment_overview_user_filter_preference'] = array(
            'null' => NULL_NOT_ALLOWED,
            'default' => BLOCK_ASSIGNMENTS_OVERVIEW_ASSIGNMENTS_DUE,
            'type' => PARAM_ALPHA,
            'choices' => array(
                    BLOCK_ASSIGNMENTS_OVERVIEW_ASSIGNMENTS_DUE,
                    BLOCK_ASSIGNMENTS_OVERVIEW_PAST_ASSIGNMENTS_SUBMITTED,
                    BLOCK_ASSIGNMENTS_OVERVIEW_PAST_ASSIGNMENTS_NOT_SUBMITTED,
                    BLOCK_ASSIGNMENTS_OVERVIEW_ASSIGNMENTS_NO_DUE_DATE,
                    BLOCK_ASSIGNMENTS_OVERVIEW_ASSIGNMENTS_ALL
            )
    );

    return $preferences;
}


/**
 * Formats the assignments for display.
 *
 * @param array $assignments   The set of assignments to display
 * @param array $allcourses    All the courses the student is on
 * @return string The assignments as HTML ready for display.
 */
function block_assignment_overview_format_assignment_block($assignments, $allcourses) {
    $config = get_config("block_assignment_overview");

    $text = '';

    $table = new html_table();
    $table->head = array(get_string('assignmenttabasscol', 'block_assignment_overview'));
    $table->attributes = array('class' => 'generaltable block-assignments-overview-table');
    $table->data = array();

    $rows = array();

    foreach ($assignments as $shortname => $mod) {
        $line = array();
        $coursetitle = '<h3>' . $shortname . '</h3>';
        $line[] = html_writer::tag('div', $coursetitle . $mod['displaytext'],
                array('class' => 'block-assignments-overview-table-new-activity'));
        $rows[] = $line;
    }
    if (count($rows) > 0) {
        $table->data = $rows;
        $text = html_writer::table($table);
    } else {
        $text = get_string('nocurrentassignments', 'block_assignment_overview');
    }
    $tabtoptext = '';

    if (isset($config->assignmentstoptext)) {
        $tabtoptext = html_writer::tag('div', $config->assignmentstoptext, array());
    }
    return $tabtoptext . $text;
}

/**
 * Get an overview of assignments activity per course for the current user. This can include Turnitin 2 assignments.
 *
 * @param array  $courses An array of courses that the user is on
 * @return array An array of the assignment activity for each course since the users last access
 */
function block_assignment_overview_get_all_assignments($courses) {
    global $CFG, $USER, $DB;

    $htmlarray = array();
    $newhtmlarray = array();

    // Store all course $cm instances for all courses.
    $allcoursecms = array();

    $assignmentslist = array();

    $tiiassignments = array();
    $gettiiassignments = false;

    // Turnitintooltwo assignments.
    $config = get_config("block_assignment_overview");
    if (!empty($config->includeturnitintooltwoassignments)) {

        // Check if TII two plugin is actually installed first.
        if (file_exists($CFG->dirroot . "/mod/turnitintooltwo/version.php")) {
            turnitintooltwo_get_assignments($courses, $tiiassignments);
            if (!empty ($tiiassignments) ) {
                $gettiiassignments = true;
            }
        }
    }

    foreach ($courses as $course) {

        if (isset($USER->lastcourseaccess[$course->id])) {
            $course->lastaccess = $USER->lastcourseaccess[$course->id];
        } else {
            $course->lastaccess = 0;
        }
        $course->mods = array();

        // For each course get mods.
        $modinfo = get_fast_modinfo($course);

        $recentactivity = array();

        foreach ($modinfo->cms as $cm) {

            // Exclude activities which are not visible or have no link.
            if (!$cm->uservisible or !$cm->has_view()) {
                continue;
            }

            if (( $cm->modname == 'assign')) {
                $metadata = \block_assignment_overview\activity::course_section_cm_get_data($cm, $course);

                if (is_array($metadata)) {
                    $metadata['coursename'] = $course->shortname;
                    $assignmentslist[] = $metadata;
                }
            } else if ( $cm->modname == 'turnitintooltwo') {
                if ($gettiiassignments) {

                    foreach ($tiiassignments[$course->id][$cm->id] as $tiiassignmentpart) {
                        $metadata = \block_assignment_overview\activity::course_section_cm_get_data($cm,
                                        $course, $tiiassignmentpart);
                        if (is_array($metadata)) {
                            $metadata['coursename'] = $course->shortname;
                            $assignmentslist[] = $metadata;
                        }
                    }
                }
            }

        }

    }

    return $assignmentslist;
}

/**
 * This is a standard Moodle module that prints out a summary of all activities of this kind for the My Moodle page for a user
 *
 * @param object $courses
 * @param object $htmlarray
 * @return bool success
 */
function turnitintooltwo_get_assignments($courses, &$htmlarray) {
    global $USER, $CFG, $DB, $OUTPUT;

    if (empty($courses) || !is_array($courses) || count($courses) == 0) {
        return array();
    }

    if (!$turnitintooltwos = get_all_instances_in_courses('turnitintooltwo', $courses)) {
        return;
    }

    $submissioncount = array();
    foreach ($turnitintooltwos as $turnitintooltwo) {
        $turnitintooltwoassignment = new turnitintooltwo_assignment($turnitintooltwo->id, $turnitintooltwo);
        $parts = $turnitintooltwoassignment->get_parts(false);

        $cm = get_coursemodule_from_id('turnitintooltwo', $turnitintooltwo->coursemodule);
        $context = context_module::instance($cm->id);

        $partsarray = array();
        $grader = has_capability('mod/turnitintooltwo:grade', $context);
        if ($grader) {
            $submissionsquery = $DB->get_records_select('turnitintooltwo_submissions',
                    'turnitintooltwoid = ? GROUP BY id, submission_part, submission_grade, submission_gmimaged',
                    array($turnitintooltwo->id), '', 'id, submission_part, submission_grade, submission_gmimaged');
            foreach ($submissionsquery as $submission) {
                if (!isset($submissioncount[$submission->submission_part])) {
                    $submissioncount[$submission->submission_part] = array('graded' => 0, 'submitted' => 0);
                }
                if ($submission->submission_grade != 'NULL' and $submission->submission_gmimaged == 1) {
                    $submissioncount[$submission->submission_part]['graded']++;
                }
                $submissioncount[$submission->submission_part]['submitted']++;
            }
        }

        foreach ($parts as $part) {

            if (!isset($submissioncount[$part->id])) {
                $submissioncount[$part->id] = array('graded' => 0, 'submitted' => 0);
            }

            $htmlarray[$turnitintooltwo->course][$cm->id][$part->id]['name'] = $cm->name . ': ' . $part->partname;
            $htmlarray[$turnitintooltwo->course][$cm->id][$part->id]['dtdue'] = $part->dtdue;
            $htmlarray[$turnitintooltwo->course][$cm->id][$part->id]['dtstart'] = $part->dtstart;

            if ($grader) {
                // If user is a grader.
                $numsubmissions = $submissioncount[$part->id]['submitted'];
                $graded = $submissioncount[$part->id]['graded'];
                $input = new stdClass();
                $input->submitted = $numsubmissions;
                $input->graded = $graded;
                $input->total = count_enrolled_users($context, 'mod/turnitintooltwo:submit', 0);
                $input->gplural = ($graded != 1) ? 's' : '';
                $partsarray[$part->id]['status'] = get_string('tutorstatus', 'turnitintooltwo', $input);

                // This is constructed for use through the usual activity and activity_meta class utilised
                // for normal assignments.
                $htmlarray[$turnitintooltwo->course][$cm->id][$part->id]['numsubmissions'] = $numsubmissions;
                $htmlarray[$turnitintooltwo->course][$cm->id][$part->id]['graded'] = $graded;
                $htmlarray[$turnitintooltwo->course][$cm->id][$part->id]['totalusers'] = $input->total;
            } else {
                // If user is a student.
                $submission = $turnitintooltwoassignment->get_submissions($cm, $part->id, $USER->id, 1);

                if (!empty($submission[$part->id][$USER->id])) {

                    $htmlarray[$turnitintooltwo->course][$cm->id][$part->id]['submitted'] = true;

                    $input = new stdClass();
                    $input->modifiedtext = userdate($submission[$part->id][$USER->id]->submission_modified,
                            get_string('strftimedatetimeshort', 'langconfig'));
                    $input->modified = $submission[$part->id][$USER->id]->submission_modified;
                    $input->objectid = $submission[$part->id][$USER->id]->submission_objectid;

                    $htmlarray[$turnitintooltwo->course][$cm->id][$part->id]['submissioninfo']  = 'Paper ID: ' . $input->objectid;

                    $htmlarray[$turnitintooltwo->course][$cm->id][$part->id]['modified'] = $input->modified;
                } else {

                    $htmlarray[$turnitintooltwo->course][$cm->id][$part->id]['submissioninfo'] =
                        get_string('nosubmissions', 'turnitintooltwo');
                    $htmlarray[$turnitintooltwo->course][$cm->id][$part->id]['submitted'] = false;
                }
            }
        }

        $attributes["class"] = ($turnitintooltwo->visible ? "" : "dimmed");
        $attributes["title"] = get_string('modulename', 'turnitintooltwo');
        $assignmentlink = html_writer::link($CFG->wwwroot."/mod/turnitintooltwo/view.php?id=".$turnitintooltwo->coursemodule,
                $turnitintooltwo->name, $attributes);

        $partsblock = "";
    }
}
